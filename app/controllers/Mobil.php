<?php 

class Mobil extends Controller{
	public function index(){
		$data['judul'] = 'Daftar Mobil';
		$data['mobil'] = $this->model('Mobil_model')->getAllMobil();
		$this->view('templates/header', $data);
		$this->view('mobil/index', $data);
		$this->view('templates/footer');
	}

	public function detail($id){
		$data['judul'] = 'Detail Mobil';
		$data['mobil'] = $this->model('Mobil_model')->getMobilById($id);
		$this->view('templates/header', $data);
		$this->view('mobil/detail', $data);
		$this->view('templates/footer');
	}

	public function hapus($id){
		$this->model('Mobil_model')->deleteById($id);
		$this->index();
	}

	public function ubah($id){
		$data = array(
			'judul' => 'Ubah Mobil',
			'mobil' => $this->model('Mobil_model')->getMobilById($id)
		);
		$this->view('templates/header', $data);
		$this->view('mobil/ubah', $data);
		$this->view('templates/footer');
	}
}